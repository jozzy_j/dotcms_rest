package com.dotcms.plugin.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import com.dotcms.repackage.javax.portlet.PortletException;
import com.dotcms.repackage.javax.portlet.PortletMode;
import com.dotcms.repackage.javax.portlet.WindowState;
import com.dotcms.repackage.javax.ws.rs.core.Response;
import com.dotcms.repackage.javax.ws.rs.core.Response.ResponseBuilder;
import com.dotcms.repackage.org.apache.commons.io.IOUtils;
import com.dotcms.repackage.org.apache.struts.action.ActionMapping;
import com.dotcms.repackage.org.codehaus.jettison.json.JSONException;
import com.dotcms.repackage.org.codehaus.jettison.json.JSONObject;
import com.dotmarketing.factories.InodeFactory;
import com.dotmarketing.portlets.structure.model.Field;
import com.dotmarketing.portlets.structure.model.Field.FieldType;
import com.dotmarketing.portlets.structure.struts.FieldForm;
import com.dotmarketing.portlets.structure.struts.StructureForm;
import com.liferay.portal.SystemException;
import com.liferay.portal.ejb.PortletManagerUtil;
import com.liferay.portal.model.Portlet;
import com.liferay.portal.model.User;
import com.liferay.portal.util.WebKeys;
import com.liferay.portlet.ActionRequestImpl;
import com.liferay.util.ParamUtil;

public class RequestHandler {
	
	
	/**
	 * @param request
	 * @return
	 * @throws JSONException
	 * @throws IOException
	 * @throws SystemException
	 * @throws Exception
	 */
	public static Response createContentType(HttpServletRequest request)
			throws JSONException, IOException, SystemException, Exception {
		HashMap<String, String> map = new HashMap<String, String>();
		JSONObject obj = new JSONObject(IOUtils.toString(request
				.getInputStream()));
		Iterator<String> keys = obj.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			Object value = obj.get(key);
			map.put(key, value.toString());
		}
		StructureForm form = new StructureForm();
		ActionMapping mapping = new ActionMapping();
		form.setDescription(map.get(Constants.DESC));
		form.setName(map.get(Constants.NAME));
		form.setStructureType(1);
		;
		Portlet portlet = PortletManagerUtil.getPortletById(Constants.COMPANYID,
				Constants.PORTLETID);
		WindowState windowState = new WindowState(ParamUtil.getString(request,
				"p_p_state"));
		PortletMode portletMode = new PortletMode(ParamUtil.getString(request,
				"p_p_mode"));
		ActionRequestImpl actionRequest = new ActionRequestImpl(request,
				portlet, null, null, windowState, portletMode, null, null);
		com.dotcms.plugin.rest.EditStructureAction createType = new com.dotcms.plugin.rest.EditStructureAction();
		com.dotmarketing.portlets.structure.model.Structure struct = createType
				.processAction(form, null, actionRequest, null);

		ResponseBuilder builder = Response.ok(
				"{\"inode\":\"" + struct.getInode() + " \"}",
				"application/json");
		return builder.build();
	}
	
	static void setUser(User user) {
		user.setCompanyId(Constants.COMPANYID);
		user.setEmailAddress(Constants.EMAIL);
		user.setFirstName(Constants.FIRSTNAME);
		user.setUserId(Constants.USERID);
	}
	
	/**
	 * @param request
	 * @return
	 */
	static Response getContentTypeFields(HttpServletRequest request) {
	
		
		ArrayList<String> list=new ArrayList<String>();
		for(FieldType fld:Field.FieldType.values()){
			list.add(fld.toString());
			
		}
	
		ResponseBuilder builder = Response.ok("{\"result\":\""  + list + "\"}", "application/json");
		return builder.build();
	}
	
	
	
	static Response getContentTypes(HttpServletRequest request) {
		
		InodeFactory inodeFact=new InodeFactory();
	   List<com.dotmarketing.portlets.structure.model.Structure> inodeClass= inodeFact.getInodesOfClass(com.dotmarketing.portlets.structure.model.Structure.class, 20);
	    ArrayList<String> list=new ArrayList<String>();
	    for(com.dotmarketing.portlets.structure.model.Structure val:inodeClass){
	    	list.add(val.getVelocityVarName());
	    }
			
		ResponseBuilder builder = Response.ok("{\"result\":\"" + list + "\"}", "application/json");
		return builder.build();
	}
	/**
	 * @param request
	 * @return
	 * @throws SystemException
	 * @throws PortletException
	 * @throws Exception
	 */
	public static Response addField(HttpServletRequest request,FieldForm form)
			throws SystemException, PortletException, Exception {
		InodeFactory abc=new InodeFactory();
		ActionMapping mapping =new ActionMapping();
	Portlet portlet =
			PortletManagerUtil.getPortletById("dotcms.org", "EXT_STRUCTURE");
	ServletContext ctx = (ServletContext)request.getAttribute(WebKeys.CTX);
	WindowState windowState = new WindowState(
			ParamUtil.getString(request, "p_p_state"));
	PortletMode portletMode = new PortletMode(
			ParamUtil.getString(request, "p_p_mode"));
	ActionRequestImpl actionRequest = new ActionRequestImpl(
			request, portlet, null, null, windowState,
			portletMode, null, null);

	EditFieldAction edit=new EditFieldAction();
	edit.processAction(mapping, form, null, actionRequest, null);	
	   InodeFactory saad=new InodeFactory();
	   List<com.dotmarketing.portlets.structure.model.Structure> sad= saad.getInodesOfClass(com.dotmarketing.portlets.structure.model.Field.class, 100);
	 
		ResponseBuilder builder = Response.ok("{\"result\":\"" +"Success" + "\"}", "application/json");
		return builder.build();
	}
	

}
