package com.dotcms.plugin.rest;

public class Constants {
	
	
	public static final String COMPANYID="dotcms.org";
	public static final String PORTLETID="EXT_STRUCTURE";
	public static final String DESC="description";
	public static final String NAME="name";
	public static final String EMAIL="admin@dotcms.com";
	public static final String FIRSTNAME="Admin";
	public static final String USERID="dotcms.org.1";
	public static final String FIELD="field";
	public static final String BINARY="binary";
	public static final String WYSIWYG="wysiwyg";
	public static final String FIELDNAME="field name";
	public static final String VELOCITYVAR="velocityvarname";
	public static final String DATATYPE="datatype";



}
