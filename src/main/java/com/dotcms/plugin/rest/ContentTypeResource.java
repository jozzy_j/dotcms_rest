package com.dotcms.plugin.rest;

import java.util.HashMap;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import com.dotcms.repackage.javax.ws.rs.GET;
import com.dotcms.repackage.javax.ws.rs.POST;
import com.dotcms.repackage.javax.ws.rs.Path;
import com.dotcms.repackage.javax.ws.rs.PathParam;
import com.dotcms.repackage.javax.ws.rs.Produces;
import com.dotcms.repackage.javax.ws.rs.core.Context;
import com.dotcms.repackage.javax.ws.rs.core.MediaType;
import com.dotcms.repackage.javax.ws.rs.core.Response;
import com.dotcms.repackage.org.apache.commons.io.IOUtils;
import com.dotcms.repackage.org.codehaus.jettison.json.JSONObject;
import com.dotcms.rest.WebResource;
import com.dotmarketing.business.DotStateException;
import com.dotmarketing.exception.DotDataException;
import com.dotmarketing.exception.DotSecurityException;
import com.dotmarketing.portlets.structure.struts.FieldForm;

@Path("/dotcms")
public class ContentTypeResource extends WebResource {


	@POST
	@Path("/contenttype")
	public Response doPost(@Context HttpServletRequest request,
			@PathParam("params") String params) throws Exception {
		return RequestHandler.createContentType(request);
	}

	@POST
	@Path("/contenttype/field")
	public Response loadRoot(@Context HttpServletRequest request,
			@PathParam("params") String params) throws Exception {
		Response response = null;
		   FieldForm form=new FieldForm();
		HashMap<String, String> map = new HashMap<String, String>();
		JSONObject obj = new JSONObject(IOUtils.toString(request
				.getInputStream()));
		Iterator<String> keys = obj.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			Object value = obj.get(key);
			map.put(key, value.toString());
		}
		if(map.get(Constants.FIELD).equals(Constants.BINARY)){
			   form.setDataType(map.get(Constants.DATATYPE));
			   form.setFieldType(Constants.BINARY);
			   form.setElement(Constants.FIELD);
			   form.setFieldName(map.get(Constants.FIELDNAME));
			   form.setVelocityVarName(map.get(Constants.VELOCITYVAR));
			response= RequestHandler.addField(request,form);
			
		}
		else if(map.get(Constants.FIELD).equals(Constants.WYSIWYG)){
			   form.setDataType(map.get(Constants.DATATYPE));
			   form.setFieldType(Constants.WYSIWYG);
			   form.setElement(Constants.FIELD);
			   form.setFieldName(map.get(Constants.FIELDNAME));
			   form.setVelocityVarName(map.get(Constants.VELOCITYVAR));
			response= RequestHandler.addField(request,form);
			
		}
		return response;

	}

	@GET
	@Path("/contenttype/fields")
	public Response loadContentTypes(@Context HttpServletRequest request,
			@PathParam("params") String params) throws DotStateException,
			DotDataException, DotSecurityException {
		return RequestHandler.getContentTypeFields(request);

	}

}