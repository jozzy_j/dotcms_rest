package com.dotcms.plugin.rest;

import com.dotcms.repackage.org.osgi.framework.BundleContext;
import com.dotcms.rest.config.RestServiceUtil;
import com.dotmarketing.osgi.GenericBundleActivator;
import com.dotmarketing.util.Logger;

public class Activator extends GenericBundleActivator {

	Class contentType = ContentTypeResource.class;
	Class content = ContentResource.class;

	public void start(BundleContext context) throws Exception {

		Logger.info(this.getClass(), "Adding new Restful Service:" + contentType.getSimpleName());
		RestServiceUtil.addResource(contentType);
		RestServiceUtil.addResource(content);

	}

	public void stop(BundleContext context) throws Exception {

		Logger.info(this.getClass(), "Removing new Restful Service:" + contentType.getSimpleName());
		RestServiceUtil.removeResource(contentType);
		RestServiceUtil.removeResource(content);

	}

}